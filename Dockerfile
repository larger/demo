FROM alpine:edge

RUN apk add --update x11vnc --allow-untrusted && \
	apk add --update alpine-base sudo xvfb libx11 randrproto xineramaproto lxdm libc6-compat fluxbox firefox-esr \
    font-misc-misc font-dec-misc font-cronyx-cyrillic font-misc-cyrillic font-screen-cyrillic font-winitzki-cyrillic \
    font-bh-lucidatypewriter-100dpi font-bh-lucidatypewriter-75dpi xf86-video-fbdev \
    xf86-video-dummy xf86-input-synaptics xf86-input-evdev && \
    rm -rf /var/cache/apk/*

RUN mkdir -p ~/.vnc && x11vnc -storepasswd coer34 ~/.vnc/passwd
EXPOSE 5900
RUN echo "firefox" >> /.bashrc